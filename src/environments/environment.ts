// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  LARAVEL: 'http://localhost/api/',
  SPRINGBOOT: 'http://localhost/machine-learning/',
  firebaseConfig: {
    apiKey: "AIzaSyAm0eTzNejVQA-EvWRSFEN4nAoyh5N3RUo",
    authDomain: "tfg-data.firebaseapp.com",
    projectId: "tfg-data",
    storageBucket: "tfg-data.appspot.com",
    messagingSenderId: "270923432467",
    appId: "1:270923432467:web:45cdcceb985d04bb4875e6",
    measurementId: "G-JFZ8N26EZ9"
  },
  // spotifyConfig: {
  //   client_credentials: "client_credentials",
  //   client_id: "a441daa012484efcb644933d67911620",
  //   client_secret: "d067c62433494ad48c90b13c89a7b494"
  // }
  spotifyConfig: {
    client_credentials: "client_credentials",
    client_id: "3388ccb0ae6442fb827e380aadb29d7a",
    client_secret: "012c366ff81941408b50575e42752a10"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
