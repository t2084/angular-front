import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PaisesService {

  paises: [any];
  constructor( private http: HttpClient ) {
    this.paises = this.obtenerPaises();
  }

  obtenerPaises(): any
  {
    return this.http.get( 'https://restcountries.com/v3.1/all' );
  }

  getPaises(): any {
    return this.paises;
  }
}
