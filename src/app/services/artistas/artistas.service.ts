import { Injectable } from '@angular/core';
import { ArtistModule } from '../../models/artist/artist.module';

@Injectable({
  providedIn: 'root'
})
export class ArtistasService {

  artistas: ArtistModule[] = [];
  constructor() { }
  crearArtista( artista: any ): ArtistModule
  {
    let newArtist: ArtistModule;
    newArtist = new ArtistModule();

    if ( artista.images.length > 0 ) {
      newArtist.rutaImagen = artista.images[0].url ;
    }
    newArtist.nombre = artista.name;
    newArtist.generos = artista.genres;
    newArtist.id = artista.id;

    this.artistas.push(newArtist);
    return newArtist;
  }

  obtenerArtista(): ArtistModule
  {
    return this.artistas[0];
  }
  obtenerArtistas(): ArtistModule[]
  {
    return this.artistas;
  }

  vaciarListado(): void
  {
    this.artistas = [];
  }
}
