import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SupervisedService {
  private API = environment.SPRINGBOOT;

  constructor( private http: HttpClient ) { }

  knn( tableName:String, knownProperties:String[] ): Observable<any> {
    return this.http.get( this.API + 'supervised/' + `knn?tableName=${tableName}&knownProperties=${knownProperties}`,
      {responseType: 'text'});
  }

  naiveBayes ( tableName:String, userId:String, songId:String ): Observable<any> {
    return this.http.get( this.API  + `supervised/naiveBayes?tableName=${tableName}&userId=${userId}&objectiveId=${songId}`,
      {responseType: 'text'})
  }
}
