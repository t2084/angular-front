import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RecommendationService {
  private API = environment.SPRINGBOOT;

  constructor( private http: HttpClient ) { }

  linearRegression( tableName:String, fields: String ): Observable<any> {
    return this.http.get( this.API + 'prediction/' + `linearRegression?tableName=${tableName}&fields=${fields}`,
      {responseType: 'text'});
  }

  knn( tableName:String, knownProperties:String[], missingProperty:String ): Observable<any> {
    return this.http.get( this.API + 'prediction/' +
      `knn?tableName=${tableName}&knownProperties=${knownProperties}&missingProperty=${missingProperty}`,
      {responseType: 'text'});
  }

  naiveBayes( tableName:String, givenInformation:String[], predictValues:String[] ): Observable<any> {
    return this.http.get( this.API + 'prediction/' +
      `naiveBayes?tableName=${tableName}&givenInformation=${givenInformation}&predictValues=${predictValues}`,
      {responseType: 'text'});
  }

  kmeans ( tableName:String ): Observable<any> {
    return this.http.get( this.API + 'prediction/' + `kmeans?tableName=${tableName}`, {responseType: 'text'});
  }
}
