import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UnsupervisedService {
  private API = environment.SPRINGBOOT;

  constructor( private http: HttpClient ) { }

  contentBasedFiltering( userId:String, numItems: number ): Observable<any> {
    return this.http.get( this.API + 'unsupervised/' + `contentBasedFiltering?userId=${userId}&numItems=${numItems}`,
      {responseType: 'text'})
  }

  collaborativeFiltering( userId:String, numItems: number ): Observable<any> {
    return this.http.get( this.API + 'unsupervised/' + `collaborativeFiltering?userId=${userId}&numItems=${numItems}`,
      {responseType: 'text'})
  }

}
