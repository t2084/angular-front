import { Injectable } from '@angular/core';
import firebase from 'firebase/compat/app';
import 'firebase/compat/storage';
import { initializeApp } from "firebase/app";
// import { getFirestore, collection, getDocs, addDoc } from 'firebase/firestore/lite';

import { environment } from '../../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class FirebaseService {

  private storageRef: any
  constructor() {
    firebase.initializeApp(environment.firebaseConfig);
    this.storageRef = firebase.app().storage().ref();
  }

  async subirImagen(nombre: string, img64: any){
    try {
      let response = await this.storageRef.child(`imagenes/${nombre}`).putString(img64, 'data_url');
      return await response.ref.getDownloadURL();
    } catch (err) {
      console.error ( err );
      return null;
    }
  }

}



