import { Injectable } from '@angular/core';
import { UsuarioModule } from '../../models/usuario/usuario.module';
import { environment } from '../../../environments/environment';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import {Observable, Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UsuariosService {

  public usuario: UsuarioModule;
  private API = environment.LARAVEL;
  public logger = new Subject<boolean>();
  public isAdmin = new Subject<boolean>();
  public loggedIn = false;
  public correo = localStorage.getItem('correo');

  constructor( private http: HttpClient ) {
    if (this.correo) {
      this.loggedIn = true;
      this.logger.next(this.loggedIn);
    } else {
      this.loggedIn = false;
      this.logger.next(this.loggedIn);
    }

    if ( localStorage.getItem('admin')) {
      this.isAdmin.next(true);
    }
  }

  sesionIniciada(): Observable<boolean> {
    return this.logger.asObservable();
  }

  crearUsuario( usuario: UsuarioModule ): Observable<any> {
    let params = new HttpParams();

    params = params.append( 'name', usuario.nombre );
    params = params.append( 'email', usuario.correo );
    params = params.append( 'password', usuario.password );
    params = params.append( 'edad', String(usuario.edad) );
    params = params.append( 'pais', usuario.pais );
    params = params.append( 'sexo', usuario.sexo );

    return this.http.post( this.API + `usuarios/nuevoUsuario/${usuario.nombre}/${usuario.correo}/${usuario.password}/${usuario.edad}/${usuario.pais}/${usuario.sexo}`, params);
  }

  encontrarUsuarioCorreo(): Observable<any> {
    return this.http.get(this.API + `usuarios/encontrarUsuario/${ localStorage.getItem('correo') }`);
  }

  encontrarUsuario( usuario: UsuarioModule ): Observable<any> {
    return this.http.get(this.API + `usuarios/encontrarUsuario/${ usuario.correo }/${ usuario.password }`);
  }

  subirFoto ( foto: any, usuario: UsuarioModule ): Observable<any> {
    let params = new HttpParams();
    params = params.append( 'foto', foto );
    return this.http.post( this.API + `usuarios/editarAvatarUsuario/${usuario.correo}/foto`, params );
  }

  editarPerfil( usuario: UsuarioModule ): Observable<any> {
    let params = new HttpParams();
    params = params.append( 'nuevoPassword', usuario.nombre );
    params = params.append( 'nuevoNombre', usuario.password );

    return this.http.post( this.API + `usuarios/editarUsuario/${usuario.correo}/${usuario.nombre}/${usuario.password}`, params);
  }

  cancionesGustadas( correo: string ): Observable<any> {
    return this.http.get( this.API + `canciones/cancionesgustadas/${correo}` );
  }

  busquedaUsuario ( campo: string ): Observable<any> {
    return this.http.get( this.API + `usuarios/getUsuario/${campo}` );
  }

}
