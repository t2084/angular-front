import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {CancionesService} from '../canciones/canciones.service';
import {ArtistasService} from '../artistas/artistas.service';
import {Observable, OperatorFunction} from 'rxjs';
import { environment } from 'src/environments/environment';
import { CancionModule } from 'src/app/models/cancion/cancion.module';

@Injectable({
  providedIn: 'root'
})
export class SpotifyService {

  public paisesDisponibles = [
    // Africa
    'Algeria', 'Angola', 'Benin', 'Botswana', 'Burkina Faso', 'Burundi', 'Cameroon', 'Cape Verde',
    'Chad', 'Comoros', 'Côte d\'Ivoire', 'Djibouti', 'Egypt', 'Equatorial Guinea', 'Eswatini', 'Gabon', 'Gambia', 'Ghana', 'Guinea',
    'Guinea-Bissau', 'Kenya', 'Lesotho', 'Liberia', 'Madagascar', 'Malawi', 'Mali', 'Mauritania', 'Mauritius', 'Morocco',
    'Mozambique', 'Namibia', 'Niger', 'Nigeria', 'Rwanda', 'São Tomé and Príncipe', 'Senegal', 'Seychelles', 'Sierra Leone',
    'South Africa', 'Tanzania', 'Togo', 'Tunisia', 'Uganda', 'Zambia', 'Zimbabwe',

    // Asia
    'Armenia', 'Azerbaijan', 'Bahrain', 'Bangladesh', 'Bhutan', 'Brunei Darussalam', 'Cambodia', 'Georgia', 'Hong Kong', 'India',
    'Indonesia', 'Israel', 'Japan', 'Jordan', 'Kuwait', 'Kyrgyzstan', 'Lao People\s Democratic Republic', 'Lebanon', 'Macao',
    'Malaysia', 'Maldives', 'Mongolia', 'Nepal', 'Oman', 'Pakistan', 'Palestine', 'Philippines', 'Qatar', 'Saudi Arabia', 'Singapore',
    'South Korea', 'Sri Lanka', 'Taiwan', 'Thailand', 'Timor-Leste', 'United Arab Emirates', 'Uzbekistan', 'Vietnam',

    // Europa
    'Andorra', 'Austria', 'Belgium', 'Bulgaria', 'Cyprus', 'Czech Republic', 'Denmark', 'Estonia', 'Finland', 'France', 'Germany',
    'Greece', 'Hungary', 'Iceland', 'Ireland', 'Italy', 'Latvia', 'Liechtenstein', 'Lithuania', 'Luxembourg', 'Malta', 'Monaco',
    'Netherlands', 'Norway', 'Poland', 'Portugal', 'Romania', 'Slovakia', 'Spain', 'Sweden', 'Switzerland', 'Turkey', 'United Kingdom',
    'Russia', 'Belarus', 'Kazakhstan', 'Moldova', 'Ukraine', 'Albania', 'Bosnia', 'Croatia', 'Montenegro', 'North Macedonia',
    'San Marino', 'Serbia', 'Slovenia', 'Kosovo',

    // Norte America
    'Antigua and Barbuda', 'Bahamas', 'Barbados', 'Belize', 'Canada', 'Costa Rica', 'Curaçao', 'Dominica', 'Dominican Republic',
    'El Salvador', 'Grenada', 'Guatemala', 'Haiti', 'Honduras', 'Jamaica', 'Mexico', 'Nicaragua', 'Panama', 'St. Kitts and Nevis',
    'St. Lucia', 'St. Vincent and the Grenadines', 'Trinidad and Tobago', 'United States of America',

    // Sudamerica
    'Argentina', 'Bolivia', 'Brazil', 'Chile', 'Colombia', 'Ecuador', 'Guyana', 'Paraguay', 'Peru', 'Suriname', 'Uruguay',

    // Oceania
    'Australia', 'Fiji', 'Kiribati', 'Marshall Islands', 'Micronesia', 'Nauru', 'New Zealand', 'Palau', 'Papua New Guinea',
    'Samoa', 'Solomon Islands', 'Tonga', 'Tuvalu', 'Vanuatu'
  ];

  private tokenType: string;
  private accessToken: string;
  private API = environment;

  constructor(private http: HttpClient,
              private cancionService: CancionesService,
              private artistaService: ArtistasService) {

    if ( !localStorage.getItem('token-type') && !localStorage.getItem('access-token') ) {
      this.actualizarToken();
    } else {
      this.tokenType = localStorage.getItem( 'token-type');
      this.accessToken = localStorage.getItem( 'access-token');
    }
  }

  private obtenerToken(): Observable<any> {
    let params = new HttpParams();

    params = params.append('grant_type', this.API.spotifyConfig.client_credentials);
    params = params.append('client_id', this.API.spotifyConfig.client_id);
    params = params.append('client_secret', this.API.spotifyConfig.client_secret);

    return this.http.post('https://accounts.spotify.com/api/token', params);
  }

  private actualizarToken ( ){
    this.obtenerToken().subscribe ( data => {
      this.tokenType = data.token_type;
      this.accessToken = data.access_token;
      localStorage.setItem( 'token-type', this.tokenType );
      localStorage.setItem( 'access-token', this.accessToken );
    });
  }

  public ejecutarConsulta(query: string): Observable<any> {
    const url = `https://api.spotify.com/v1/${query}`;
     const headers: HttpHeaders = new HttpHeaders(
       {
         Authorization: this.tokenType + " " + this.accessToken
       }
     );

     return this.http.get(url, { headers });
  }

  public obtenerUltimosLanzamientos(pais: string): void {
    this.cancionService.vaciarListado();
    let lastReleases: any;
    let query: { pipe: (arg0: OperatorFunction<any, any>) => { subscribe: (arg0: (data: any) => void) => void; }; };

    query = this.ejecutarConsulta(`browse/new-releases?country=${pais}&limit=4`);
    lastReleases = query.pipe(map((data: any) => data.albums.items));

      lastReleases.subscribe((data) => {
        data.forEach(album => {
            this.cancionService.addTopTrack(album);
          }
        );
      }, ( err ) => this.actualizarToken());

  }

  public obtenerArtista(id: string): any {
    this.artistaService.vaciarListado();
    return this.ejecutarConsulta(`artists/${id}`);
  }

  public obtenerTopTracksArtista(id: string): any {
    let genre: any;
    const topTracks = this.ejecutarConsulta(`artists/${id}/top-tracks?market=ES`).pipe(map((data: any) => data.tracks));

    this.cancionService.vaciarListado();
    topTracks.subscribe(data => {
      data.forEach(cancion => {
        this.buscarArtistasId(cancion.artists[0].id).subscribe( data => {
          this.cancionService.addCancion(cancion, data.genres[0]);
        } );
      });
    }, ( err ) => this.actualizarToken());
  }

  public async buscarCancionesPorId(id: string) {
    const tracks = this.ejecutarConsulta(`tracks/${id}?market=ES`);
    let genre: any;
    let cancion: CancionModule;
    this.cancionService.vaciarListado();
    tracks.subscribe( cancion => {
      this.buscarArtistasId(cancion.artists[0].id).subscribe( (data) => {
        this.cancionService.addCancion(cancion, data.genres[0]);
      } ) ;
    }, ( err ) => this.actualizarToken());
  }

  public async buscarDeTuEstiloPorId(id: string) {
    const tracks = this.ejecutarConsulta(`tracks/${id}?market=ES`);
    let genre: any;
    let cancion: CancionModule;
    this.cancionService.vaciarDeTuEstilo();
    tracks.subscribe( cancion => {
      this.buscarArtistasId(cancion.artists[0].id).subscribe( (data) => {
        this.cancionService.addDeTuEstilo(cancion, data.genres[0]);
      } ) ;
    }, ( err ) => this.actualizarToken());
  }

  public async buscarGustadasPorUsuariosSimilares(id: string) {
    const tracks = this.ejecutarConsulta(`tracks/${id}?market=ES`);
    let genre: any;
    let cancion: CancionModule;
    this.cancionService.vaciarGustadasPorUsuariosSimilares();
    tracks.subscribe( cancion => {
      this.buscarArtistasId(cancion.artists[0].id).subscribe( (data) => {
        this.cancionService.addGustadasPorUsuariosSimilares(cancion, data.genres[0]);
      } ) ;
    }, ( err ) => this.actualizarToken());
  }

  public async buscarSimilaresPorId(id: string) {
    const tracks = this.ejecutarConsulta(`tracks/${id}?market=ES`);
    let genre: any;
    let cancion: CancionModule;
    tracks.subscribe( cancion => {
      this.buscarArtistasId(cancion.artists[0].id).subscribe( (data) => {
        this.cancionService.addRecomendacion(cancion, data.genres[0]);
      } ) ;
    }, ( err ) => this.actualizarToken());
  }

  buscarArtistasId(id: string): Observable<any> {
    return this.ejecutarConsulta(`artists/${id}`);
  }

  buscarCancionId(id:string): Observable<any> {
    return this.ejecutarConsulta(`tracks/${id}`);
  }

  public buscarCanciones(termino: string): void {
    let genre: any;
    this.cancionService.vaciarListado();
    const canciones = this.buscar(termino, 'track');

    canciones.subscribe(data => {
      data.forEach(cancion => {
        /* Para obtener el género */
        this.buscarArtistasId( cancion.artists[0].id ).subscribe( data => {
          this.cancionService.addCancion(cancion, data.genres[0]);
        } );
      });
    }, ( err ) => this.actualizarToken());
  }

  public buscarArtistas(termino: string): void {
    this.artistaService.vaciarListado();
    const artistas = this.buscar(termino, 'artist');

    artistas.subscribe(data => {
      data.forEach(artista => {
        this.artistaService.crearArtista(artista);
      });
    }, ( err ) => this.actualizarToken());
  }

  private buscar(termino: string, item: string): any {
    const search = this.ejecutarConsulta(`search?q=${termino}&type=${item}&limit=15`);
    if (item === 'artist') {
      return search.pipe(map((data: any) => data.artists.items));
    }
    if (item === 'track') {
      return search.pipe(map((data: any) => data.tracks.items));
    }
  }
}
