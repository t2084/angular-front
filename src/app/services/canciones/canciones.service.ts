import { Injectable } from '@angular/core';
import {CancionModule} from '../../models/cancion/cancion.module';
import {environment} from '../../../environments/environment';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import { RecommendationService } from '../recommendation/recommendation.service';
import { SupervisedService } from '../recommendation/supervised.service';
import { UnsupervisedService } from '../recommendation/unsupervised.service';

@Injectable({
  providedIn: 'root'
})
export class CancionesService {

  private API = environment.LARAVEL;
  private topTrackCounter = 0;
  canciones: CancionModule[];
  recomendaciones: CancionModule[];
  deTuEstilo: CancionModule[];
  gustadasPorUsuariosSimilares: CancionModule[];
  constructor( private http: HttpClient,
               private supervisedService: SupervisedService,
               private unsupervisedService: UnsupervisedService,
               private recommendationService: RecommendationService ) {
    this.canciones = [];
    this.recomendaciones = [];
    this.deTuEstilo = [];
  }

  addTopTrack( album: any ): void
  {
      const newCancion = new CancionModule();
      newCancion.titulo = album.name;
      newCancion.idArtista = album.artists[0].id;
      newCancion.artista = album.artists[0].name;
      newCancion.duracion = 2;
      newCancion.id = album.id;

      if ( album.images) {
        newCancion.rutaImagen = album.images[0].url;
      } else if (album.album.images ) {
        newCancion.rutaImagen = album.album.images[0].url;
      }

      newCancion.idArtista = album.artists[0].id;
      this.canciones.push( newCancion );
      this.topTrackCounter ++;

      this.almacenarCancion( newCancion ).subscribe( () => {}, error => { console.error( 'La cancion ya existia' ); });
  }

  addDeTuEstilo( cancion: any, genre: string )
  {
    const newCancion = new CancionModule();

    newCancion.titulo = cancion.name;
    newCancion.idArtista = cancion.artists[0].id;
    newCancion.artista = cancion.artists[0].name;
    newCancion.duracion = cancion.duration_ms;
    newCancion.genero = genre;
    newCancion.id = cancion.id;

    if ( cancion.images) {
      newCancion.rutaImagen = cancion.images[0].url;
    } else if (cancion.album.images ) {
      newCancion.rutaImagen = cancion.album.images[0].url;
    }

    newCancion.idArtista = cancion.artists[0].id;
    this.deTuEstilo.push( newCancion );
  }

  addGustadasPorUsuariosSimilares( cancion: any, genre: string )
  {
    const newCancion = new CancionModule();

    newCancion.titulo = cancion.name;
    newCancion.idArtista = cancion.artists[0].id;
    newCancion.artista = cancion.artists[0].name;
    newCancion.duracion = cancion.duration_ms;
    newCancion.genero = genre;
    newCancion.id = cancion.id;

    if ( cancion.images) {
      newCancion.rutaImagen = cancion.images[0].url;
    } else if (cancion.album.images ) {
      newCancion.rutaImagen = cancion.album.images[0].url;
    }

    newCancion.idArtista = cancion.artists[0].id;
    this.gustadasPorUsuariosSimilares.push( newCancion );
  }

  addRecomendacion( cancion: any, genre: string )
  {
    const newCancion = new CancionModule();

    newCancion.titulo = cancion.name;
    newCancion.idArtista = cancion.artists[0].id;
    newCancion.artista = cancion.artists[0].name;
    newCancion.duracion = cancion.duration_ms;
    newCancion.genero = genre;
    newCancion.id = cancion.id;

    if ( cancion.images) {
      newCancion.rutaImagen = cancion.images[0].url;
    } else if (cancion.album.images ) {
      newCancion.rutaImagen = cancion.album.images[0].url;
    }

    newCancion.idArtista = cancion.artists[0].id;
    this.recomendaciones.push( newCancion );
  }

  addCancion( cancion: any, genre: string )
  {
    const newCancion = new CancionModule();

    newCancion.titulo = cancion.name;
    newCancion.idArtista = cancion.artists[0].id;
    newCancion.artista = cancion.artists[0].name;
    newCancion.duracion = cancion.duration_ms;
    newCancion.genero = genre;
    newCancion.id = cancion.id;

    if ( cancion.images) {
      newCancion.rutaImagen = cancion.images[0].url;
    } else if (cancion.album.images ) {
      newCancion.rutaImagen = cancion.album.images[0].url;
    }

    newCancion.idArtista = cancion.artists[0].id;
    this.canciones.push( newCancion );

    this.almacenarCancion( newCancion ).subscribe( () => {}, error => { console.error( 'La cancion ya existia' ); });
  }

  obtenerCanciones(): CancionModule[]
  {
    return this.canciones;
  }

  obtenerRecomendaciones(): CancionModule[]
  {
    return this.recomendaciones;
  }
  vaciarListado(): void
  {
    this.canciones = [];
  }

  vaciarListadoRecomendaciones(){
    this.recomendaciones = [];
  }

  vaciarDeTuEstilo() {
    this.deTuEstilo = [];
  }

  vaciarGustadasPorUsuariosSimilares() {
    this.gustadasPorUsuariosSimilares = [];
  }

  almacenarCancion( cancion: CancionModule ): Observable<any> {
    let params = new HttpParams();
    params = params.append( 'songId', cancion.id);
    params = params.append( 'title', cancion.titulo);
    params = params.append( 'duration', cancion.duracion.toString());
    params = params.append( 'genreId', cancion.genero);
    params = params.append( 'artistId', cancion.idArtista.toString());
    return this.http.post( this.API + `canciones/addCancion/${cancion.id}/${cancion.titulo}/${cancion.duracion.toString()}/${cancion.idArtista.toString()}/${cancion.genero}`,
      params);
  }

  valorarCancion( cancion: CancionModule, usuarioEmail: string, gustada: boolean): Observable<any> {
    let params = new HttpParams();
    params = params.append( 'cancionId', cancion.id.toString());
    params = params.append( 'email', usuarioEmail);
    params = params.append( 'liked', Number(gustada).toString());
    return this.http.post( this.API + `canciones/canciongustada/${cancion.id.toString()}/${usuarioEmail}/${Number(gustada).toString()}`, params);
  }

  desvalorarCancion( cancion: CancionModule, usuarioEmail: string, gustada: boolean): Observable<any> {
    let params = new HttpParams();
    params = params.append( 'cancionId', cancion.id.toString());
    params = params.append( 'email', usuarioEmail);
    params = params.append( 'liked', Number(gustada).toString());
    return this.http.post( this.API + `canciones/cancionnogustada/${cancion.id.toString()}/${usuarioEmail}/${Number(gustada).toString()}`, params);
  }

  esCancionGustada ( cancion: CancionModule, usuarioEmail: string ): Observable<any> {
    return this.http.get(environment.LARAVEL + `canciones/escanciongustada/${cancion.id.toString()}/${usuarioEmail}`)
  }

  busquedaCancion ( campo: string ): Observable<any> {
    return this.http.get( this.API  + `canciones/getCancion/${campo}`);
  }

  busquedaCancionSimilar ( id:string ): Observable<any> {
    let fieldsAlg = [];
    fieldsAlg.push("songId" + "=" + id);
    return this.supervisedService.knn("canciones", fieldsAlg);
  }

  contentBasedFiltering ( usuarioId, numItems ): Observable<any> {
    return this.unsupervisedService.contentBasedFiltering( usuarioId, numItems );
  }

  collaborativeFiltering ( usuarioId, numItems ): Observable<any> {
    return this.unsupervisedService.collaborativeFiltering( usuarioId, numItems );
  }

  calcularProbabilidadGustar( usuarioId:string, id:string ): Observable<any> {
    return this.supervisedService.naiveBayes( "canciones", usuarioId, id );
  }

}
