import { Component } from '@angular/core';
import { UsuariosService } from './services/usuarios/usuarios.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'SpotiApp';

  public loggedIn =  localStorage.getItem('correo') ? true : false;
  public isAdmin = localStorage.getItem('admin') ? true : false;
  constructor(public usuarioService: UsuariosService) {
    this.usuarioService.isAdmin.subscribe( data =>  this.isAdmin = data);
    this.usuarioService.logger.subscribe(data => this.loggedIn = data);
  }


}
