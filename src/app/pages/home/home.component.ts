import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'; // Para redireccionar a la pagina del artista-perfil
import { CancionModule } from '../../models/cancion/cancion.module';
import { UsuarioModule } from '../../models/usuario/usuario.module';
import { CancionesService } from '../../services/canciones/canciones.service';
import { UsuariosService } from '../../services/usuarios/usuarios.service';
import { SpotifyService } from '../../services/spotify/spotify.service';
import { PaisesService } from '../../services/paises/paises.service';
import { async } from 'rxjs';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent{
  usuario: UsuarioModule;
  novedadesPersonalizadas: CancionModule[];
  deTuEstilo: CancionModule[];
  countries: any[] = [];
  selectedCountry: any;
  loading = true;

  constructor( private usuarioService: UsuariosService,
               private cancionesServicio: CancionesService,
               private spotifyServicio: SpotifyService,
               private router: Router,
               private countryServices: PaisesService
            ) {

        this.countryServices.getPaises().subscribe(data => {
          for ( const country of data ) {
            if ( spotifyServicio.paisesDisponibles.includes(country.name.common) ) {
              this.countries.push( country );
              this.countries = this.countries.sort((a, b) => {
                return ( a.name.common > b.name.common ) ?  1 : ( a.name.common < b.name.common ) ? -1 : 0;
              });
            }
          }

          this.selectedCountry = this.countries.find( country => country.name.common === 'Spain' ).cca2;
          this.actualizarNovedades();
          this.loading = false;
        });
        this.usuario = new UsuarioModule();

        this.usuarioService.encontrarUsuarioCorreo().subscribe( data => {
          this.usuario.id = data.userId;
          this.obtenerCancionesDeTuEstilo();
          this.usuario.correo = data.email;
          this.usuario.nombre = data.name;
          this.usuario.password = data.password;
          this.usuario.pais = data.country;
          this.usuario.sexo = data.sex;
        });

  }

  actualizarNovedades(): void
  {
    this.cargarNovedades();
    this.cargarCanciones();
  }

  cargarCanciones(): void
  {
    this.novedadesPersonalizadas = [];
    this.novedadesPersonalizadas = this.cancionesServicio.obtenerCanciones();
  }

  cargarNovedades(): void
  {
    this.spotifyServicio.obtenerUltimosLanzamientos( this.selectedCountry );
  }

  verCreadoParaTi(): void {
    this.router.navigate(['/creado-para-ti', this.usuario.id]);
  }

  obtenerCancionesDeTuEstilo(): void {

    this.deTuEstilo = [];
    this.cancionesServicio.contentBasedFiltering(this.usuario.id, 4).subscribe( data => {
      let result = JSON.parse(data);
      let cancionesId = Object.keys(result);
      for ( let cancionId of cancionesId ) {
        this.spotifyServicio.buscarDeTuEstiloPorId( cancionId );
      }

      this.deTuEstilo = this.cancionesServicio.deTuEstilo;
    } );
  }

}
