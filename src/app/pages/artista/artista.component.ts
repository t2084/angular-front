import {Component, Input, OnInit} from '@angular/core';
import {ArtistModule} from '../../models/artist/artist.module';
import {Router} from '@angular/router';

@Component({
  selector: 'app-artista',
  templateUrl: './artista.component.html',
  styleUrls: ['./artista.component.css']
})
export class ArtistaComponent{


  @Input() artista: ArtistModule;
  constructor( private router: Router ) { }

  abrirArtista( id: string ): void
  {
    this.router.navigate(['/artist', id]);
  }
}
