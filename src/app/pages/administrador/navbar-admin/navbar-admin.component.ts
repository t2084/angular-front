import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UsuariosService } from 'src/app/services/usuarios/usuarios.service';

@Component({
  selector: 'app-navbar-admin',
  templateUrl: './navbar-admin.component.html',
  styleUrls: ['./navbar-admin.component.css']
})
export class NavbarAdminComponent implements OnInit {

  constructor(public usuarioService: UsuariosService, private router: Router) { }

  ngOnInit(): void {
  }


  logout(): void {
    localStorage.removeItem('correo');
    localStorage.removeItem('admin');
    this.usuarioService.loggedIn = false;
    this.usuarioService.logger.next(this.usuarioService.loggedIn);
    this.usuarioService.isAdmin.next(false);
    this.router.navigateByUrl('login'); /* Mensaje de aviso de que se ha cerrado la sesion */
  }

}
