import { Component, OnInit } from '@angular/core';
import { UsuariosService } from '../../services/usuarios/usuarios.service';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';
import { TableModule } from 'src/app/models/table/table.module';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-administrador',
  templateUrl: './administrador.component.html',
  styleUrls: ['./administrador.component.css']
})

export class AdministradorComponent {
  message: String = "";
  registros: String[] = [];
  tablas: String[] = [];
  nombreTablas: String[] = [];
  valores: String[] = [];

  constructor(
    private http: HttpClient
  ) {
    this.encontrarTablas();
  }

  encontrarTablas(): void {
    this.message = "";
    this.registros = [];
    this.http.get(environment.LARAVEL + `tablas/listartablas`).subscribe((data: any) => {
      for (const table of data) {
        const tableName: string = table.Tables_in_laravelapp;
        if ((tableName.includes('canciones') || tableName.includes('usuarios')) &&
          (!tableName.includes("ml_"))) {
          let tabla = new TableModule();
          tabla.nombre = tableName;
          this.tablas[tableName] = [];
          this.valores[tableName] = [];
          this.nombreTablas.push( tabla.nombre );
          this.encontrarCamposTabla ( tableName );
        }
      }
    });
  }

  encontrarCamposTabla(nombreTabla: string): void {
    this.http.get(environment.LARAVEL + `tablas/listarCampos/${nombreTabla}`).subscribe((data: any) => {
      for (var field of data) {
        if (field == "created_at" || field == "updated_at" || ( field.toLowerCase() == "id" || field.toLowerCase() == "songId" )
          || field.includes("password") || field.includes('title') || field.includes("name") || field.includes("mail")
          || field.includes("isAdmin")) continue;
        let campo = new TableModule();
        campo.nombre = field;

        this.tablas[nombreTabla].push(campo.nombre);
        // this.valores[nombreTabla][campo.nombre] = [];
      }
    });
  }

  actualizarImportancia(form: NgForm): void {
    if ( !form.valid ) {
      Object.values( form.controls ).forEach(
        control => {
          control.markAsTouched();
        }
      );
      return ;
    }
    this.establecerImportancias();
  }

  establecerImportancias() {
    let params = new HttpParams();
    params = params.append( 'importancias', this.valores.toString());

    let tableName = "";
    let field = "";
    let importance = "";

    for ( let key in this.valores ) {
      tableName = key;
      for (let valueKey in this.valores[key]) {
        field = valueKey;
        importance = this.valores[key][valueKey];
        this.http.post(environment.LARAVEL + `tablas/establecerImportanciaTablas/${tableName}/${field}/${importance}`, params)
          .subscribe( data => console.log ( data ) );
      }
    }
  }

}
