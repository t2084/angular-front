import { Component, OnInit } from '@angular/core';
import {UsuariosService} from '../../../services/usuarios/usuarios.service';
import {Router} from '@angular/router';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-algoritmos',
  templateUrl: './algoritmos.component.html',
  styleUrls: ['./algoritmos.component.css']
})
export class AlgoritmosComponent {

  loading = false;
  constructor( public usuarioService: UsuariosService, private router: Router,
               private http: HttpClient ) {
  }

  irAlgoritmo(nombre: string): void {
    if ( nombre === 'Regresión lineal' ) {
      this.router.navigate(['/algoritmo/linearregression', nombre]);
    } else if ( nombre === 'K-Vecinos más cercanos' ) {
      this.router.navigate(['/algoritmo/knn', nombre]);
    } else if ( nombre === 'Naive Bayes' ) {
      this.router.navigate(['/algoritmo/naivebayes', nombre]);
    } else if ( nombre === 'K-Means' ) {
      this.router.navigate(['/algoritmo/kmeans', nombre]);
    } else {
      this.router.navigate(['/prueba']);
    }

  }

  irAlgoritmoColab(nombre: string): void{
    this.router.navigate(['/algoritmoColab', nombre]);
  }

  irAlgoritmoContent(nombre: string): void{
    this.router.navigate(['/algoritmoCont', nombre]);
  }

}
