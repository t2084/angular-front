import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TableModule } from 'src/app/models/table/table.module';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-colaborativos',
  templateUrl: './colaborativos.component.html',
  styleUrls: ['./colaborativos.component.css']
})
export class ColaborativosComponent {

  nombreAlgoritmo: string;
  tablas = [];
  fields = new Map();

  constructor(private route: ActivatedRoute, private http: HttpClient) {
    this.route.params.subscribe(params => {
      this.nombreAlgoritmo = params.name;
    });
    this.encontrarTablas();
  }

  encontrarTablas(): void {
    this.http.get(environment.LARAVEL + `tablas/listartablas`).subscribe((data: any) => {
      for (const table of data) {
        const tableName: string = table.Tables_in_laravelapp;
        if ((tableName.includes('canciones') || tableName.includes('usuarios')) &&
          (!tableName.includes("ml_"))) {
          let tabla = new TableModule();
          tabla.nombre = tableName;
          this.tablas.push(tabla);
          this.encontrarCamposTabla( tabla.nombre ).then ( data => this.fields.set( tabla.nombre, data ));
        }
      }
    });
  }

  async encontrarCamposTabla(nombreTabla: String): Promise<any[]> {
    let campos: any[] = [];
    let request = await fetch(environment.LARAVEL + `listarCampos/${nombreTabla}`);
    request.json().then( data => data.forEach( row => {
      if ( row !== "row_names" ) {
        campos.push( row );
      }
    } ));
    return campos;

  }

}
