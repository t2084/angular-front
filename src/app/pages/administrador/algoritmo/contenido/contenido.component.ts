import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TableModule } from 'src/app/models/table/table.module';
import { SupervisedService } from 'src/app/services/recommendation/supervised.service';
import { environment } from 'src/environments/environment';
import { UsuarioModule } from '../../../../models/usuario/usuario.module';

@Component({
  selector: 'app-contenido',
  templateUrl: './contenido.component.html',
  styleUrls: ['./contenido.component.css']
})
export class ContenidoComponent {

  nombreAlgoritmo: string;
  tablas = [];
  fields = [];
  registros = [];
  status: number;
  loading: boolean;
  message = "";
  nombreTabla = "";
  fieldsAlg = [];
  userFound: UsuarioModule;
  initialCount = 0; steps = 10;
  error = false;

  constructor(
    private route: ActivatedRoute,
    private http: HttpClient,
    private supervisedService: SupervisedService ) {
    this.route.params.subscribe(params => {
      this.nombreAlgoritmo = params.name;
    });
    this.encontrarTablas();
  }

  encontrarTablas(): void {
    this.initialCount = 0; this.steps = 10;

    this.http.get( environment.LARAVEL + `tablas/listartablas`).subscribe((data: any) => {
      for (const table of data) {
        const tableName: string = table.Tables_in_laravelapp;
        if ((tableName.includes('canciones') || tableName.includes('usuarios')) &&
          (!tableName.includes("ml_"))) {
          let tabla = new TableModule();
          tabla.nombre = tableName;
          this.tablas.push(tabla);
        }
      }
    });
  }

  async seleccionarInputTabla(nombreTabla: string) {
    this.fields    = [];
    this.registros = [];
    this.nombreTabla = nombreTabla;
    let tableRequest = await this.encontrarCamposTabla( nombreTabla ).then ( data => this.fields = data );
    let registersRequest = await this.encontrarRegistrosTabla( nombreTabla ).then ( data => this.registros = data );
  }

  async encontrarRegistrosTabla(nombreTabla: String): Promise<any[]> {
    let registros = []
    let count = 0;
    let id = 0;
    let request = await fetch(environment.LARAVEL + `tablas/listarRegistros/${nombreTabla}/${this.initialCount}/${this.steps}` );
    request.json().then( data => data.forEach( registro => {
      registros.push(registro);
    }));
    return registros;
  }

  async encontrarCamposTabla(nombreTabla: String): Promise<any[]> {
    let campos: any[] = [];
    let request = await fetch(environment.LARAVEL + `tablas/listarCampos/${nombreTabla}`);
    request.json().then( data => data.forEach( row => {
      if ( row !== "row_names" ) {
        campos.push( row );
      }
    } ));
    return campos;
  }

  seleccionarRegistro(registro: any): void {
    let registroString = JSON.stringify(registro);
    registroString = registroString.substring(1, registroString.length-2);
    let entries = registroString.split(",");
    let key, value = "";
    this.fieldsAlg = [];

    entries.forEach( entry => {
      key = entry.split(":")[0];
      value = entry.split(":")[1];

      key = key.replace(/"/g, '');
      value = value.replace(/"/g, '');

      if ( key === 'sex_female' ) {
        this.fieldsAlg.push("sex" + "=" + value);
      }

      if ( key !== "row_names" && key !== 'sex_male' && key !== 'sex_female') {
        this.fieldsAlg.push(key + "=" + value);
      }
    })
  }

  ejecutarAlgoritmo(): void {
    this.loading = true;
    this.message = "";

    this.supervisedService.knn(this.nombreTabla, this.fieldsAlg).subscribe( data => {
      this.message = data;
      this.loading = false;
      this.buscarEntidad(this.message);
    }, error => {
      this.message = error.error.text;
      this.error = true;
      this.buscarEntidad(this.message);
      this.loading = false;
    })

  }

  buscarEntidad(datos: String): void {
    let data = datos.substring(1, datos.length-1);
    let frag = data.split(",");
    let id = frag[0].split("=")[1];

    if ( this.nombreTabla === "usuarios" ) {
      this.userFound = new UsuarioModule();
      this.http.get(environment.LARAVEL + `usuarios/encontrarUsuarioId/${id}`).subscribe( (data:any) => {
        this.userFound.correo = data.email;
        this.userFound.nombre = data.name;
        this.userFound.edad = data.age;
        this.userFound.pais = data.country;
        this.userFound.avatar = data.avatar;
      });
    }

  }
}
