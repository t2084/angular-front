import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TableModule } from '../../../../models/table/table.module';
import { RecommendationService } from '../../../../services/recommendation/recommendation.service';
import { environment } from '../../../../../environments/environment';

@Component({
  selector: 'app-naivebayes',
  templateUrl: './naivebayes.component.html',
  styleUrls: ['./naivebayes.component.css']
})
export class NaivebayesComponent {

  nombreAlgoritmo = '';
  tablas = [];
  campos = [];
  registros = [];
  message = "";
  nombreTabla = ""
  loading = false
  selectedTable = ""
  nombresCampos = []
  numCamposSelect = 0;
  selectRegistro: any;
  numberOfFieldsWritten = 0;
  valoresFields = [];
  maxBlankSpacesError = false;
  selectedSex = "";
  fieldsValue = new Map();
  knownValue = "";
  updateRecommendation = false;
  initialCount = 0; steps = 10;
  error= false;

  constructor(private route: ActivatedRoute,
              private http: HttpClient,
              private recommendationService: RecommendationService) {
    this.route.params.subscribe(params => {
      this.nombreAlgoritmo = params.name;
    });

    this.encontrarTablas();
  }

  encontrarTablas(): void {
    this.initialCount = 0; this.steps = 10;
    this.message = "";
    this.fieldsValue = new Map();
    this.registros = [];
    this.http.get(environment.LARAVEL + `tablas/listartablas`).subscribe((data: any) => {
      for (const table of data) {
        const tableName: string = table.Tables_in_laravelapp;
        if ((tableName.includes('canciones') || tableName.includes('usuarios')) &&
          (!tableName.includes("ml_"))) {
          let tabla = new TableModule();
          tabla.nombre = tableName;
          this.tablas.push(tabla);
        }
      }
    });
  }

  async actualizarDatos() {
    this.loading = true;
    this.http.get(environment.LARAVEL + 'updateData').subscribe((data) => {
      this.updateRecommendation = false;
      this.loading = false;
    });
  }

  encontrarCamposTabla(nombreTabla: string): void {
    this.message = "";
    this.fieldsValue = new Map();
    this.registros = [];
    this.selectedTable = nombreTabla;
    this.campos = [];
    this.nombreTabla = nombreTabla;
    this.nombresCampos = [];
    this.valoresFields = [];

    this.http.get(environment.LARAVEL + `tablas/listarCampos/${nombreTabla}`).subscribe((data: [any]) => {
      for (var field of data) {
        if (field == "created_at" || field == "updated_at" || field.toLowerCase().includes("id") || field.includes("password")
          || field.includes('title') || field.includes("name") || field.includes("mail") || field.includes("isAdmin")) continue;
        let campo = new TableModule();
        campo.nombre = field;
        this.campos.push(campo);
        this.nombresCampos.push(campo.nombre);
        if (!field.includes('sex')) {
          this.fieldsValue.set(campo.nombre, "");
        }
      }

      for (let i = 0; i < this.filterNoSexFields().length; i++) {
        this.valoresFields.push("");
      }

      if ( this.campos.length === 0 ) {
        this.updateRecommendation = true;
      }
    });
  }

  filterNoSexFields(): any[] {
    return this.nombresCampos.filter(data => !data.includes('sex'));
  }

  getFieldRegisters(nombreTabla: string): void {
    this.http.get(environment.LARAVEL + `tablas/listarRegistros/${nombreTabla}/${this.initialCount}/${this.steps}`)
      .subscribe((data: [any]) => {
        this.registros = data;
        if ( this.registros.length == 0 ) {
          this.updateRecommendation = true;
        }
      });
  }

  getCheckedValues(): TableModule[] {
    return this.campos.filter((value, index) => {
      if (value.checked) {
        return value;
      }
    });
  }

  guardarValores(campo: string, valor: string): void {

    if (campo.includes("sex") && valor === "1") {
      if (this.selectedSex !== "") {
        this.message = "Ya hay un sexo";
        return;
      }
    }

    if (campo.includes("sex")) {
      if (campo === "sex_female") {
        if (valor === "1") {
          this.selectedSex = "female";
        } else {
          this.selectedSex = "";
        }
      } else if (campo === "sex_male") {
        if (valor === "1") {
          this.selectedSex = "male";
        } else {
          this.selectedSex = "";
        }
      }
    } else {
      this.fieldsValue.set(campo, valor);
    }

    if (!this.checkIntegridad(campo, valor)) {
      this.fieldsValue.delete(campo);
    }
  }

  checkIntegridad(campo: string, valor: string): boolean {
    let count = 0;
    this.message = "";

    if (this.selectedSex !== "") {
      count++;
    }

    for (var [key, value] of this.fieldsValue) {
      if (value.length > 0) {
        count++;
      }
      if (count > 2) {
        this.message = "Solo debe rellenar dos campos";
        return false;
      }
    }
    return true;
  }

  ejecutarAlgoritmo() {
    this.loading = true;
    this.message = "";
    let knownField = [];
    let uknownField = [];

    for ( let [key, value] of this.fieldsValue ) {
      if (value !== "") {
        if (key === this.knownValue) {
          knownField.push(key + "=" + value);
        } else {
          uknownField.push(key + "=" + value);
        }
      }
    }
    if ( knownField.length == 0 && this.selectedSex !== "" ) {
      knownField.push ("sex" + "=" + this.selectedSex);
    }

    if ( uknownField.length == 0 && this.selectedSex !== "" ) {
      uknownField.push ("sex" + "=" + this.selectedSex);
    }

    this.recommendationService.naiveBayes( this.nombreTabla, knownField, uknownField ).subscribe( data => {
     this.message = "La probabilidad es: " + data;
     this.error = false;
     this.loading = false;
    }, error => {
      this.message = error.error.text;
      this.error = true;
      this.loading = false;
    } );
  }
}
