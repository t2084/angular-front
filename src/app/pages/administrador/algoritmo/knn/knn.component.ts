import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TableModule } from 'src/app/models/table/table.module';
import { RecommendationService } from 'src/app/services/recommendation/recommendation.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-knn',
  templateUrl: './knn.component.html',
  styleUrls: ['./knn.component.css']
})
export class KnnComponent {

  nombreAlgoritmo = '';
  tablas = [];
  campos = [];
  registros = [];
  message = "";
  error = false;
  nombreTabla = ""
  loading = false
  selectedTable = ""
  nombresCampos = []
  numCamposSelect = 0;
  selectRegistro: any;
  numberOfFieldsWritten = 0;
  valoresFields = [];
  maxBlankSpacesError = false;
  selectedSex = "";
  fieldsValue = new Map();
  updateRecommendation = false;
  missingProperty = "";

  initialCount = 0; steps = 10;
  constructor(
    private route: ActivatedRoute,
    private http: HttpClient,
    private recommendationService: RecommendationService
  ) {
    this.route.params.subscribe(params => {
      this.nombreAlgoritmo = params.name;
    });

    this.encontrarTablas();
  }

  encontrarTablas(): void {
    this.initialCount = 0; this.steps = 10;
    this.message = "";
    this.fieldsValue = new Map();
    this.registros = [];
    this.http.get(environment.LARAVEL + `tablas/listartablas`).subscribe((data: any) => {
      for (const table of data) {
        const tableName: string = table.Tables_in_laravelapp;
        if ((tableName.includes('canciones') || tableName.includes('usuarios')) &&
          (!tableName.includes("ml_"))) {
          let tabla = new TableModule();
          tabla.nombre = tableName;
          this.tablas.push(tabla);
        }
      }
    });
  }

  async actualizarDatos() {
    this.loading = true;
    this.http.get(environment.LARAVEL + 'repositorio/updateData').subscribe((data) => {
      this.updateRecommendation = false;
      this.loading = false;
    });
  }

  encontrarCamposTabla(nombreTabla: string): void {
    this.message = "";
    this.fieldsValue = new Map();
    this.registros = [];
    this.selectedTable = nombreTabla;
    this.campos = [];
    this.nombreTabla = nombreTabla;
    this.nombresCampos = [];
    this.valoresFields = [];

    this.http.get(environment.LARAVEL + `tablas/listarCampos/${nombreTabla}`).subscribe((data: [any]) => {
      for (var field of data) {
        if (field == "created_at" || field == "updated_at" || field.toLowerCase().includes("id") || field.includes("password")
          || field.includes('title') || field.includes("name") || field.includes("mail") || field.includes("isAdmin")) continue;
        let campo = new TableModule();
        campo.nombre = field;
        this.campos.push(campo);
        this.nombresCampos.push(campo.nombre);
        if (!field.includes('sex')) {
          this.fieldsValue.set(campo.nombre, "");
        }
      }

      for (let i = 0; i < this.filterNoSexFields().length; i++) {
        this.valoresFields.push("");
      }

      if ( this.campos.length === 0 ) {
        this.updateRecommendation = true;
      }
    });
  }


  filterNoSexFields(): any[] {
    return this.nombresCampos.filter(data => !data.includes('sex'));
  }

  filterSexFields(): any[] {
    return this.nombresCampos.filter(data => data.includes('sex'));
  }

  getFieldRegisters(nombreTabla: string): void {
    this.http.get(environment.LARAVEL + `tablas/listarRegistros/${nombreTabla}/${this.initialCount}/${this.steps}`)
      .subscribe((data: [any]) => {
        this.registros = data;
        if ( this.registros.length == 0 ) {
          this.updateRecommendation = true;
        }
      });
  }

  getCheckedValues(): TableModule[] {
    return this.campos.filter((value, index) => {
      if (value.checked) {
        return value;
      }
    });
  }

  mostrarPopUp(): void {
    document.getElementById('pop-up').style.display = 'block';
  }

  ocultarPopUp(): void {
    document.getElementById('pop-up').style.display = 'none';
  }

  ejecutarAlgoritmo() {
    this.loading = true;
    this.message = "";
    var fields = [];
    let knownProperties = [];

    this.maxBlankSpacesError = false;
        let numEmpty = 0;
        for (var valor of this.valoresFields) {
          if (valor == "") {
            numEmpty++;
          }
        }
        if (this.selectedSex === "" && this.selectedTable == 'usuarios') {
          numEmpty++;
        }
        if (numEmpty != 1 || this.valoresFields.length < this.campos.length - this.filterSexFields().length) {
          this.maxBlankSpacesError = true;
          this.loading = false;
          return;
        }

        this.ocultarPopUp();

        for (let fValue of this.fieldsValue) {
          if ( fValue[1] ) knownProperties.push(fValue[0] + "=" + fValue[1]);
          else this.missingProperty = fValue[0];
        }

        if (this.selectedTable == 'usuarios') {
          if ( this.selectedSex !== "" ) knownProperties.push("sex" + "=" + this.selectedSex.substr(4));
          else this.missingProperty = "sex";
        }

        this.recommendationService.knn( this.nombreTabla, knownProperties, this.missingProperty ).subscribe ( data => {
          this.message = data;
          this.loading = false;
       }, error => {
          this.message = error.error.text;
          this.error = true;
          this.loading = false;
        });
  }

  guardarValores(campo: string, valor: string): void {
    if (campo.includes("sex") && valor === "1") {
      if (this.selectedSex !== "") {
        this.message = "Ya hay un sexo";
        return;
      }
    }

    if (campo.includes("sex")) {
      if (campo === "sex_female") {
        if (valor === "1") {
          this.selectedSex = "female";
        } else {
          this.selectedSex = "";
        }
      } else if (campo === "sex_male") {
        if (valor === "1") {
          this.selectedSex = "male";
        } else {
          this.selectedSex = "";
        }
      }
    } else {
      this.fieldsValue.set(campo, valor);
    }

    if (!this.checkIntegridad(campo, valor)) {
      this.fieldsValue.delete(campo);
    }

  }

  checkIntegridad(campo: string, valor: string): boolean {
    let count = 0;
    this.message = "";

    if (this.selectedSex !== "") {
      count++;
    }

    for (var [key, value] of this.fieldsValue) {
      if (value.length > 0) {
        count++;
      }
      if (count > 2) {
        this.message = "Solo debe rellenar dos campos";
        return false;
      }
    }
    return true;
  }

}
