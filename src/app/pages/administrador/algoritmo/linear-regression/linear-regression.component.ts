import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TableModule } from '../../../../models/table/table.module';
import { RecommendationService } from '../../../../services/recommendation/recommendation.service';
import { environment } from '../../../../../environments/environment';

@Component({
  selector: 'app-linear-regression',
  templateUrl: './linear-regression.component.html',
  styleUrls: ['./linear-regression.component.css']
})
export class LinearRegressionComponent {

  nombreAlgoritmo = '';
  updateRecommendation = false;
  message = '';
  fieldsValue = new Map();
  registros = [];
  selectedTable = '';
  campos = [];
  nombreTabla = '';
  nombresCampos = [];
  valoresFields = [];
  loading: boolean;
  tablas = [];
  initialCount = 0;
  steps = 10;
  error = false;

  constructor( private route: ActivatedRoute,
               private http: HttpClient,
               private recomendationService: RecommendationService ) {
      this.route.params.subscribe(params => {
        this.nombreAlgoritmo = params.name;
    });

      this.encontrarTablas();
  }

  encontrarTablas(): void {
    this.initialCount = 0; this.steps = 10;
    this.message = '';
    this.fieldsValue = new Map();
    this.registros = [];
    this.http.get(environment.LARAVEL + `tablas/listartablas`).subscribe((data: any) => {
      for (const table of data) {
        const tableName: string = table.Tables_in_laravelapp;
        if ((tableName.includes('canciones') || tableName.includes('usuarios')) &&
          (!tableName.includes('ml_'))) {
          const tabla = new TableModule();
          tabla.nombre = tableName;
          this.tablas.push(tabla);
          console.log ( tabla );
        }
      }
    });
  }

  encontrarCamposTabla(nombreTabla: string): void {
    this.message = '';
    this.fieldsValue = new Map();
    this.registros = [];
    this.selectedTable = nombreTabla;
    this.campos = [];
    this.nombreTabla = nombreTabla;
    this.nombresCampos = [];
    this.valoresFields = [];

    this.http.get(environment.LARAVEL + `tablas/listarCampos/${nombreTabla}`).subscribe((data: [any]) => {
      for (const field of data) {
        if (field == 'created_at' || field == 'updated_at' || field.toLowerCase().includes('id') || field.includes('password')
          || field.includes('title') || field.includes('name') || field.includes('mail') || field.includes('isAdmin')) { continue; }
        const campo = new TableModule();
        campo.nombre = field;
        this.campos.push(campo);
        this.nombresCampos.push(campo.nombre);
        if (!field.includes('sex')) {
          this.fieldsValue.set(campo.nombre, '');
        }
      }

      for (let i = 0; i < this.filterNoSexFields().length; i++) {
        this.valoresFields.push('');
      }

      if ( this.campos.length === 0 ) {
        this.updateRecommendation = true;
      }
    });
  }

  filterNoSexFields(): any[] {
    return this.nombresCampos.filter(data => !data.includes('sex'));
  }

  async actualizarDatos() {
    this.loading = true;
    this.http.get(environment.LARAVEL + 'repositorio/updateData').subscribe((data) => {
      this.updateRecommendation = false;
      this.loading = false;
    });
  }

  getFieldRegisters(nombreTabla: string): void {
    this.http.get(environment.LARAVEL + `tablas/listarRegistros/${nombreTabla}/${this.initialCount}/${this.steps}`)
      .subscribe((data: [any]) => {
        this.registros = data;
        if ( this.registros.length == 0 ) {
          this.updateRecommendation = true;
        }
      });
  }

  getCheckedValues(): TableModule[] {
    return this.campos.filter((value, index) => {
      if (value.checked) {
        return value;
      }
    });
  }

  ejecutarAlgoritmo(): void {
    this.loading = true;
    this.message = '';
    const fields = [];
    const selectedValues = this.getCheckedValues();

    for (const value of selectedValues) {
        fields.push(value.nombre);
      }

    this.recomendationService.linearRegression(this.nombreTabla, fields.toString()).subscribe( data => {
        this.message = data;
        this.loading = false;
      }, error => {
        this.message = error.error.text;
        this.error = true;
        this.loading = false;
      } );

  }

}
