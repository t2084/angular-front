import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CancionModule } from '../../../../models/cancion/cancion.module';
import { TableModule } from '../../../../models/table/table.module';
import { UsuarioModule } from '../../../../models/usuario/usuario.module';
import { CancionesService } from '../../../../services/canciones/canciones.service';
import { UsuariosService } from '../../../../services/usuarios/usuarios.service';
import { environment } from '../../../../../environments/environment';

@Component({
  selector: 'app-decisiontree',
  templateUrl: './decisiontree.component.html',
  styleUrls: ['./decisiontree.component.css']
})
export class DecisiontreeComponent {

  nombreAlgoritmo = '';
  tablas = [];
  campos = [];
  registros = [];
  message = "";
  loading = false
  fieldsValue = new Map();
  knownValue = "";
  updateRecommendation = false;
  foundUsers: UsuarioModule[] = [];
  foundSongs: CancionModule[] = [];
  selectedUser: UsuarioModule;
  selectedSong: CancionModule;
  status: number;

  initialCount = 0; steps = 10;
  constructor(
    private route: ActivatedRoute,
    private http: HttpClient,
    private usuarioService: UsuariosService,
    private cancionService: CancionesService
  ) {
    this.route.params.subscribe(params => {
      this.nombreAlgoritmo = params.name;
    });

    this.encontrarTablas();
  }

  async actualizarDatos() {
    this.loading = true;
    this.http.get(environment.LARAVEL + 'updateData').subscribe((data) => {
      this.updateRecommendation = false;
      this.loading = false;
    });
  }

  encontrarTablas(): void {
    this.initialCount = 0; this.steps = 10;
    this.message = "";
    this.fieldsValue = new Map();
    this.registros = [];
    this.http.get(environment.LARAVEL + `tablas/listartablas`).subscribe((data: any) => {
      for (const table of data) {
        const tableName: string = table.Tables_in_laravelapp;
        if (tableName.includes('canciones') || tableName.includes('usuarios')) {
          let tabla = new TableModule();
          tabla.nombre = tableName;
          this.tablas.push(tabla);
        }
      }
    });
  }

  buscarUsuario ( campo:any ) {
    this.foundUsers = [];
    this.usuarioService.busquedaUsuario( campo.value ).subscribe( (data:any[]) => {
      for ( let foundUser of data ) {
        let auxUser = new UsuarioModule();
        auxUser.correo = foundUser.email;
        auxUser.nombre = foundUser.name;
        auxUser.edad = foundUser.age;
        auxUser.sexo = foundUser.sex;
        auxUser.pais = foundUser.country;
        this.foundUsers.push( auxUser );
      }
    } );
  }

  buscarCancion ( campo:any ) {
    this.foundSongs = [];
    this.cancionService.busquedaCancion( campo.value ).subscribe( (data:any[]) => {
      for ( let foundSong of data ) {
        let auxSong = new CancionModule();
        auxSong.id = foundSong.id;
        auxSong.titulo = foundSong.title;
        auxSong.genero = foundSong.genre;
        auxSong.duracion = foundSong.duration;
        auxSong.artista = foundSong.artistId;
        this.foundSongs.push( auxSong );
      }
    } );
  }

  establecerUsuario ( usuario: UsuarioModule ) {
    this.selectedUser = usuario;
  }

  establecerCancion ( cancion: CancionModule ) {
    this.selectedSong = cancion;
  }

  ejecutarAlgoritmo(): void {

    /*
      Datos:
      Usuario: edad, sexo, pais
      Cancion: genero, duracion, idArtista
     */

    let userData = [];
    userData.push ( "age:" + this.selectedUser.edad );
    userData.push ( "sex:" + this.selectedUser.sexo );
    userData.push ( "country:" + this.selectedUser.pais );

    let songData = [];
    songData.push( "genre:" + this.selectedSong.genero );
    songData.push( "duration:" + this.selectedSong.duracion );
    songData.push( "idArtist:" + this.selectedSong.idArtista );

    let data = userData.toString() + ";" + songData.toString();

    this.http.get(environment.LARAVEL + `algoritmos/ejecutarAlgoritmo/decisiontree/prediction/${data.toString()}`)
      .subscribe((data: any) => {
        var response = data.split("-");
        this.message = response[0];
        this.status = response[1];
        this.loading = false;
      });
  }

}
