import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TableModule } from 'src/app/models/table/table.module';
import { RecommendationService } from 'src/app/services/recommendation/recommendation.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-kmeans',
  templateUrl: './kmeans.component.html',
  styleUrls: ['./kmeans.component.css']
})
export class KmeansComponent {

  nombreAlgoritmo = '';
  tablas = [];
  campos = [];
  registros = [];
  message = "";
  error = false;
  nombreTabla = ""
  loading = false
  selectedTable = ""

  constructor(
    private route: ActivatedRoute,
    private http: HttpClient,
    private recommendationService: RecommendationService
  ) {
    this.route.params.subscribe(params => {
      this.nombreAlgoritmo = params.name;
    });

    this.encontrarTablas();
  }

  encontrarTablas(): void {
    this.message = "";
    this.registros = [];
    this.http.get(environment.LARAVEL + `tablas/listartablas`).subscribe((data: any) => {
      for (const table of data) {
        const tableName: string = table.Tables_in_laravelapp;
        if ((tableName.includes('canciones') || tableName.includes('usuarios')) &&
          (!tableName.includes("ml_"))) {
          let tabla = new TableModule();
          tabla.nombre = tableName;
          this.tablas.push(tabla);
        }
      }
    });
  }

  ejecutarAlgoritmo(): void {
    this.error = false;
    this.recommendationService.kmeans( this.nombreTabla ).subscribe ( data => {
      this.message = data;
      this.loading = false;
    }, error => {
      this.message = error.message;
      this.error = true;
      this.loading = false;
    });
  }

}
