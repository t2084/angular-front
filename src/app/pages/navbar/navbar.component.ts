import { Component, OnInit } from '@angular/core';
import { SpotifyService } from '../../services/spotify/spotify.service';
import { Router } from '@angular/router';
import { UsuariosService } from '../../services/usuarios/usuarios.service';
import {UsuarioModule} from '../../models/usuario/usuario.module';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent {

  usuario: UsuarioModule;
  constructor( private spotifyService: SpotifyService,
               public usuarioService: UsuariosService,
               private router: Router) {
    this.sesionIniciada();
    this.usuario = new UsuarioModule();

    this.usuarioService.encontrarUsuarioCorreo().subscribe( data => {
      this.usuario.id = data.userId;
      this.usuario.correo = data.email;
      this.usuario.nombre = data.name;
      this.usuario.password = data.password;
      this.usuario.pais = data.country;
      this.usuario.sexo = data.sex;
      this.usuario.avatar = data.avatar;
    });
  }

  buscar( item: string ): void
  {
    if (item === '') {
      this.router.navigate(['/home']);
    } else {
      this.router.navigate(['/search', item]);
    }
  }
  abrirCerrarMenu( item: Element ): void
  {
    if ( item.className === 'cerrado')
    {
      item.className = 'abierto';
    } else if (item.className === 'abierto' )
    {
      item.className = 'cerrado';
    }
  }

  sesionIniciada(): void {
     this.usuarioService.sesionIniciada().subscribe( data => {
       this.usuarioService.loggedIn = data;
    } );
  }

  logout(): void {
    localStorage.removeItem('correo');
    this.usuarioService.loggedIn = false;
    this.usuarioService.logger.next(this.usuarioService.loggedIn);
    this.router.navigateByUrl('login'); /* Mensaje de aviso de que se ha cerrado la sesion */
  }


}
