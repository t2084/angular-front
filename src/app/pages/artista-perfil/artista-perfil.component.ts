import { Component, OnInit } from '@angular/core';
import { ActivatedRoute} from '@angular/router';
import { SpotifyService } from '../../services/spotify/spotify.service';
import {ArtistModule} from '../../models/artist/artist.module';
import {ArtistasService} from '../../services/artistas/artistas.service';
import {CancionModule} from '../../models/cancion/cancion.module';
import {CancionesService} from '../../services/canciones/canciones.service';

@Component({
  selector: 'app-artista-perfil',
  templateUrl: './artista-perfil.component.html',
  styleUrls: ['./artista-perfil.component.css']
})
export class ArtistaPerfilComponent implements OnInit {

  artista: ArtistModule;
  topTracks: CancionModule[];
  loading = true;
  constructor( private route: ActivatedRoute,
               private service: SpotifyService,
               private artistService: ArtistasService,
               private cancionesServicio: CancionesService) {
    this.route.params.subscribe( params =>
    {
      this.getArtista( params.id );
      this.getTopTracks( params.id );
      this.cargarCanciones();
    });
  }

  getArtista( id: string ): void
  {
     this.service.obtenerArtista( id ).subscribe( data => {
       this.artistService.crearArtista( data );
       this.artista = this.artistService.obtenerArtista();
       this.loading = false;
     });
  }

  getTopTracks( id: string ): void
  {
    this.topTracks = this.service.obtenerTopTracksArtista( id );
  }


  cargarCanciones(): void
  {
    this.topTracks = [];
    this.topTracks = this.cancionesServicio.obtenerCanciones();
  }

  ngOnInit(): void {
  }

}
