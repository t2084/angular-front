import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CancionModule } from 'src/app/models/cancion/cancion.module';
import { CancionesService } from 'src/app/services/canciones/canciones.service';

@Component({
  selector: 'app-novedad',
  templateUrl: './novedad.component.html',
  styleUrls: ['./novedad.component.css']
})
export class NovedadComponent  {

  @Input() cancion: CancionModule;
  constructor( private router: Router, private cancionService: CancionesService ) {
  }

  verArtista(id: number): void {
    this.router.navigate(['/artist', id]);
  }


}
