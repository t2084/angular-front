import { Component, OnInit } from '@angular/core';
import { SpotifyService } from '../../services/spotify/spotify.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ArtistasService } from '../../services/artistas/artistas.service';
import { ArtistModule } from '../../models/artist/artist.module';
import { CancionModule } from '../../models/cancion/cancion.module';
import { CancionesService } from '../../services/canciones/canciones.service';

@Component({
  selector: 'app-busqueda',
  templateUrl: './busqueda.component.html',
  styleUrls: ['./busqueda.component.css']
})
export class BusquedaComponent {

  resultadosArtistas: ArtistModule[];
  resultadosCanciones: CancionModule[];
  termino: string;
  loading = false;
  constructor( private spotifyService: SpotifyService,
               private route: ActivatedRoute,
               private router: Router,
               private artistaService: ArtistasService,
               private cancionService: CancionesService ) {
    this.resultadosArtistas = [];
    this.resultadosCanciones = [];

    this.route.params.subscribe( params =>
    {
      this.buscarArtistas( params.termino );
      this.buscarCanciones( params.termino );
      this.termino = params.termino;
      this.loading = false;
    });
  }

  buscarArtistas( item: string): void
  {
    this.spotifyService.buscarArtistas( item );
    this.resultadosArtistas = this.artistaService.obtenerArtistas();
  }
  buscarCanciones( item: string ): void
  {
    this.spotifyService.buscarCanciones( item );
    this.resultadosCanciones = this.cancionService.obtenerCanciones();
  }



}
