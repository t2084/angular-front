import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CancionModule } from 'src/app/models/cancion/cancion.module';
import { CancionesService } from 'src/app/services/canciones/canciones.service';
import { SpotifyService } from 'src/app/services/spotify/spotify.service';

@Component({
  selector: 'app-creado-para-ti',
  templateUrl: './creado-para-ti.component.html',
  styleUrls: ['./creado-para-ti.component.css']
})
export class CreadoParaTiComponent {

  deTuEstilo: CancionModule[];
  otrosUsuarios: CancionModule[];
  private usuarioId: string;
  constructor(
    private route: ActivatedRoute,
    private cancionesServicio: CancionesService,
    private spotifyServicio: SpotifyService
  ) {
    this.route.params.subscribe( params =>
    {
      this.usuarioId = params.usuarioId;
    });
    this.obtenerCancionesDeTuEstilo();
    this.obtenerCancionesUsuariosSimilares();
  }

  obtenerCancionesDeTuEstilo(): void {
    this.deTuEstilo = [];
    this.cancionesServicio.contentBasedFiltering(this.usuarioId, 10).subscribe( data => {
      let result = JSON.parse(data);
      let cancionesId = Object.keys(result);
      for ( let cancionId of cancionesId ) {
        this.spotifyServicio.buscarDeTuEstiloPorId( cancionId );
      }

      this.deTuEstilo = this.cancionesServicio.deTuEstilo;
    } );
  }

  obtenerCancionesUsuariosSimilares(): void {
    this.cancionesServicio.collaborativeFiltering(this.usuarioId, 10).subscribe ( data => {
      let cleanString = data.replace(/\s/g, '');
      cleanString = cleanString.replace("[", "");
      cleanString = cleanString.replace("]", "");
      let cancionesId = cleanString.split(",");
      for ( let cancionId of cancionesId ) {
        this.spotifyServicio.buscarGustadasPorUsuariosSimilares( cancionId );
      }

      this.otrosUsuarios = this.cancionesServicio.gustadasPorUsuariosSimilares;
    });
  }


}
