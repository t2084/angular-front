import { Component, Input, OnInit } from '@angular/core';
import { CancionModule } from '../../models/cancion/cancion.module';
import { Router } from '@angular/router';
import { CancionesService } from '../../services/canciones/canciones.service';
import { UsuariosService } from 'src/app/services/usuarios/usuarios.service';
import { UsuarioModule } from 'src/app/models/usuario/usuario.module';


@Component({
  selector: 'app-cancion',
  templateUrl: './cancion.component.html',
  styleUrls: ['./cancion.component.css']
})
export class CancionComponent implements OnInit{

  @Input() cancion: CancionModule;
  private usuario: UsuarioModule;
  hover = false;
  gustada: boolean;
  probability: string;
  constructor(
    private router: Router, private cancionService: CancionesService, private usuarioService: UsuariosService ) {
    this.usuario = new UsuarioModule();

    this.usuarioService.encontrarUsuarioCorreo().subscribe( data => {
      this.usuario.id = data.userId;
      this.usuario.correo = data.email;
      this.usuario.nombre = data.name;
      this.usuario.password = data.password;
      this.usuario.pais = data.country;
      this.usuario.sexo = data.sex;
    });
  }

  ngOnInit() {
    this.cancionEsGustada();
  }

  verArtista(id: number): void {
    this.router.navigate(['/artist', id]);
  }

  cancionGustada(): void {
    this.cancion.gustada = true;
    this.cancionService.valorarCancion(this.cancion, localStorage.getItem('correo'), true)
      .subscribe(data => {
        // this.router.navigate([this.router.url]);
      });
  }

  cancionNoGustada(): void {
    this.cancion.gustada = false
    this.cancionService.desvalorarCancion(this.cancion, localStorage.getItem('correo'), false)
      .subscribe(data => {
        // this.router.navigate([this.router.url]);
      });
  }

  cancionEsGustada(): void {
    this.cancionService.esCancionGustada(this.cancion, localStorage.getItem('correo'))
      .subscribe ( data => this.gustada = data );
  }

  calcularProbabilidadGustar(): void {
    this.cancionService.calcularProbabilidadGustar( this.usuario.id, this.cancion.id ).subscribe( data => {
      this.probability = data;
    });
  }

  setHoverIn(): void {
    this.hover = true;
  }
  setHoverOut(): void {
    this.hover = false;
  }

}
