import { Component, OnInit } from '@angular/core';
import {PaisesService} from '../../services/paises/paises.service';
import {UsuariosService} from '../../services/usuarios/usuarios.service';
import {UsuarioModule} from '../../models/usuario/usuario.module';
import {Router} from '@angular/router';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {

  countries: any[] = [];
  selectedCountry: any;
  usuario: UsuarioModule;
  repeatedPassword: string;
  signUpError: boolean;
  coincidenPasswords: boolean;
  constructor( private countryServices: PaisesService,
               private usuariosService: UsuariosService,
               private router: Router) {
    this.usuario = new UsuarioModule();
    this.selectedCountry = 'US';

  }
  ngOnInit(): void {
    this.countryServices.getPaises().subscribe(data => {
      this.countries = data;
    });
  }

  validar( form: NgForm ): void {
    if ( form.valid ) {
      console.log ( this.usuario );
      this.registro();
    } else {
      Object.values( form.controls ).forEach(
        control => {
          control.markAsTouched();
        }
      );
      return ;
    }
  }

  equalPasswords( password1: string, password2: string ): void {
    this.coincidenPasswords = password1 === password2;
  }

  registro(): void {
    this.usuariosService.crearUsuario( this.usuario ).subscribe( data => {
      console.log ( data );
      if (data) {
        localStorage.setItem( 'correo' , this.usuario.correo );
        this.usuariosService.loggedIn = true;
        this.usuariosService.logger.next(this.usuariosService.loggedIn);
        this.router.navigateByUrl('home');
      } else {
        this.signUpError = true;
      }
    });
  }
}
