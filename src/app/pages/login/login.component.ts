import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { PaisesService } from '../../services/paises/paises.service';
import { UsuariosService } from '../../services/usuarios/usuarios.service';
import { UsuarioModule } from '../../models/usuario/usuario.module';
import { Router } from '@angular/router';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  @ViewChild('loginButton', { read: ElementRef, static:false }) loginButton: ElementRef;
  countries: any[] = [];
  selectedCountry: any;
  usuario: UsuarioModule;
  password1: string;
  password2: string;
  error = false;

  constructor( private countryServices: PaisesService,
               private usuariosService: UsuariosService,
               private router: Router ) {
    this.password1 = 'password';
    this.password2 = 'password';
    this.usuario = new UsuarioModule();
    this.selectedCountry = 'US';
    countryServices.obtenerPaises().subscribe( data => {
      data.forEach( country => {
        this.countries.push( country );
      });
    });
  }

   iniciarSesion(): void {
    this.loginButton.nativeElement.classList.remove ("btn-primary");

    this.usuariosService.encontrarUsuario( this.usuario ).subscribe( data => {
      this.usuario.correo = data.email;
      this.usuario.sexo = data.sex;
      this.usuario.edad = data.age;
      this.usuario.pais = data.country;
      this.usuario.password = data.password;

      localStorage.setItem( 'correo' , this.usuario.correo );
      this.usuariosService.loggedIn = true;

      this.usuariosService.logger.next(this.usuariosService.loggedIn);

      if ( data.isAdmin ) {
        localStorage.setItem('admin', '1');
        this.usuariosService.isAdmin.next(true);
        this.router.navigateByUrl('administrador');
      } else {
        this.router.navigateByUrl('home');

      }
    } , error => {
      this.error = true;
      this.loginButton.nativeElement.classList.add ("btn-primary");
    } );
  }

  validar( form: NgForm ): void {
    if ( form.valid ) {
      this.iniciarSesion();
    } else {
      Object.values( form.controls ).forEach(
        control => {
          control.markAsTouched();
        }
      );
      return ;
    }
  }

  showHidePassword( whichPswd: string ): void
  {
    if ( whichPswd === 'first') {
      if ( this.password1 === 'text') {
        this.password1 = 'password';
      } else {
        this.password1 = 'text';
      }
    } else {
      if ( this.password2 === 'text') {
        this.password2 = 'password';
      } else {
        this.password2 = 'text';
      }
    }

  }


}
