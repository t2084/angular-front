import { Component, OnInit } from '@angular/core';
import { SpotifyService } from '../../services/spotify/spotify.service';
import { ArtistasService } from '../../services/artistas/artistas.service';
import Swal from 'sweetalert2';
import { CancionModule } from 'src/app/models/cancion/cancion.module';
import { CancionesService } from 'src/app/services/canciones/canciones.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-gustos-usuario',
  templateUrl: './gustos-usuario.component.html',
  styleUrls: ['./gustos-usuario.component.css']
})
export class GustosUsuarioComponent {

  canciones: CancionModule[];
  cancionesElegidas: CancionModule[];
  imagenCancionesSeleccionadas: KeyValuePair[];
  seleccionado: boolean;
  borrado: boolean;
  terminado = false;

  constructor( private spotifyService: SpotifyService,
               private cancionesService: CancionesService,
               private router: Router ) {
    this.cancionesElegidas = [];
    this.canciones = [];
    this.imagenCancionesSeleccionadas = [];
  }

  buscar( termino: string ): void {
    this.spotifyService.buscarCanciones( termino );
    this.canciones = this.cancionesService.obtenerCanciones();
  }

  seleccionarCancion( cancion: CancionModule ): void {
    if ( this.cancionesElegidas.indexOf( cancion ) === -1 ) {
      this.cancionesElegidas.push( cancion );
      this.imagenCancionesSeleccionadas.push( { key: cancion, value: cancion.rutaImagen } );

      this.seleccionado = true;
      const promise = new Promise(( resolve ) => {
        setTimeout(() => {
          this.seleccionado = false;
          resolve(1);
        }, 1500);
      });
    }
  }

  deseleccionarCancion( artista: CancionModule ): void {
    const index = this.cancionesElegidas.indexOf( artista );
    this.cancionesElegidas.splice(index, 1);
    this.reestablecerImageArtist( artista );
    this.borrado = true;

    const promise = new Promise((resolve, reject) => {
      setTimeout(() => {
        this.borrado = false;
        resolve(1);
      }, 1500);
    });
  }

  findImageByArtist( cancion: CancionModule ): string {
    for ( const listArtist of this.imagenCancionesSeleccionadas ) {
        if ( listArtist.key.id === cancion.id ) {
            return listArtist.key.rutaImagen;
        }
    }
    return null;
  }

  hoverImageArtist( cancion: CancionModule ): void {
    for ( const listArtist of this.imagenCancionesSeleccionadas ) {
      if ( listArtist.key.id === cancion.id ) {
        listArtist.key.rutaImagen = 'assets/img/deleteimage.png';
      }
    }
  }

  reestablecerImageArtist( cancion: CancionModule ): void {
    for ( const imagenCancion of this.imagenCancionesSeleccionadas ) {
      if ( imagenCancion.key.id === cancion.id ) {
        this.spotifyService.buscarCancionId( imagenCancion.key.id ).subscribe( data => {
          let ruta: string;
          if ( data.album.images.length === 0 ) {
            ruta = 'assets/img/not-found.png';
          } else {
            ruta = data.album.images[0].url;
          }
          imagenCancion.key.rutaImagen = ruta;
        });

      }
    }
  }

  valorarCanciones() {
    for ( let cancion of this.cancionesElegidas ) {
        this.cancionesService.valorarCancion( cancion, localStorage.getItem('correo'), true).subscribe ( data => {
          console.log ( data );
        });
    }
    this.router.navigate(["/"]);
  }
}

interface KeyValuePair {
  key: CancionModule;
  value: string;
}
