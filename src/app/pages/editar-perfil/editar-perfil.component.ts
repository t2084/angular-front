import { Component, OnInit } from '@angular/core';
import { UsuarioModule } from '../../models/usuario/usuario.module';
import { UsuariosService } from '../../services/usuarios/usuarios.service';
import { NgForm } from '@angular/forms';
import { CancionModule } from '../../models/cancion/cancion.module';
import { FirebaseService } from './../../services/firebase/firebase.service';

@Component({
  selector: 'app-editar-perfil',
  templateUrl: './editar-perfil.component.html',
  styleUrls: ['./editar-perfil.component.css']
})
export class EditarPerfilComponent {

  usuario: UsuarioModule;
  usuarioFormulario: UsuarioModule;
  editDone: number;
  error: boolean;
  canciones: CancionModule;
  private imagePath: string;
  imagenCargada: string|ArrayBuffer;

  constructor( private usuarioService: UsuariosService, private firebaseService: FirebaseService ) {
    this.usuario = new UsuarioModule();
    this.usuarioFormulario = new UsuarioModule();

    this.usuarioService.encontrarUsuarioCorreo().subscribe( data => {
      this.usuario.correo = data.email;
      this.usuario.nombre = data.name;
      this.usuario.password = data.password;
      this.usuario.pais = data.country;
      this.usuario.sexo = data.sex;
      this.usuario.avatar = data.avatar;
    });
  }

  subirFoto( event: any ): void {
    const file = event.target[0].files[0];
    let reader = new FileReader();

    reader.readAsDataURL(file);
    reader.onloadend = () => {
      this.imagenCargada = reader.result;
      this.firebaseService.subirImagen( `${this.usuario.correo}`, this.imagenCargada ).then( url => {
        this.imagenCargada = url;
        this.usuarioService.subirFoto(this.imagenCargada, this.usuario).subscribe( data => console.log ( data )) ;
      } );
    }
  }

  validar( form: NgForm  ): void {
    if ( form.valid ) {
      console.log('El formulario es valido');
      this.editarPerfil();
    } else {
      console.log('El formulario no es valido');
      Object.values( form.controls ).forEach(
        control => {
          control.markAsTouched();
        }
      );
      this.error = true;
      return ;
    }
  }
  editarPerfil(): void {
    if ( this.usuarioFormulario.correo === undefined) {
      this.usuarioFormulario.correo = this.usuario.correo;
    }
    if ( this.usuarioFormulario.nombre === undefined) {
      this.usuarioFormulario.nombre = this.usuario.nombre;
    }
    this.usuarioService.editarPerfil(this.usuarioFormulario).subscribe(data => this.editDone = data);
  }
}
