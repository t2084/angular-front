import { Component, OnInit } from '@angular/core';
import { UsuarioModule } from '../../models/usuario/usuario.module';
import { UsuariosService } from '../../services/usuarios/usuarios.service';
import { Router } from '@angular/router';
import { SpotifyService } from '../../services/spotify/spotify.service';
import { CancionesService } from '../../services/canciones/canciones.service';
import { CancionModule } from '../../models/cancion/cancion.module';

@Component({
  selector: 'app-mi-musica',
  templateUrl: './mi-musica.component.html',
  styleUrls: ['./mi-musica.component.css']
})
export class MiMusicaComponent implements OnInit {

  public usuario: UsuarioModule;
  public cancionesValoradas: CancionModule[];
  public cancionesSimilares: CancionModule[];
  constructor( private usuarioService: UsuariosService,
               private router: Router,
               private spotifyService: SpotifyService,
               private cancionesService: CancionesService ) {
    this.usuario = new UsuarioModule();

    this.cancionesValoradas = [];
    this.cancionesSimilares = [];
    this.usuarioService.encontrarUsuarioCorreo().subscribe( data => {
      if ( data ) {
        this.usuario.correo = data.email;
        this.usuario.nombre = data.name;
        this.usuario.password = data.password;
        this.usuario.pais = data.country;
        this.usuario.sexo = data.sex;
      } else {
        localStorage.removeItem('correo');
        this.usuarioService.loggedIn = false;
        this.usuarioService.logger.next(this.usuarioService.loggedIn);
      }
    });
  }

  ngOnInit(): void {
    this.cancionesService.vaciarListadoRecomendaciones();
    this.cancionesService.vaciarListado();
    this.cancionesSimilares = [];
    this.cancionesGustadas();
  }

  cargarCanciones(): void
  {
    this.cancionesValoradas = this.cancionesService.obtenerCanciones();
  }

  cargarRecomendaciones(): void {
    this.cancionesSimilares = this.cancionesService.obtenerRecomendaciones();
  }

  replaceAll(str, find, replace) {
    return str.replace(new RegExp(find, 'g'), replace);
  }

  cancionesGustadas(): void {
    this.usuarioService.cancionesGustadas(localStorage.getItem('correo')).subscribe( data => {
      for (const id of data ) {
        this.spotifyService.buscarCancionesPorId( id );
        this.cancionesService.busquedaCancionSimilar( id ).subscribe ( data => {
          this.parsearCancion( data );
          new Promise( resolve => setTimeout(resolve, 5000) )
        }, err => console.log ( err ) );
      }
      this.cargarCanciones();
      this.cargarRecomendaciones();
    });
  }

  parsearCancion ( cadenaJson:string ) {
    this.spotifyService.buscarSimilaresPorId( cadenaJson );
  }

}
