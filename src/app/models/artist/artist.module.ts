import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ArtistaInterface} from '../../interfaces/artista.interface';



@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class ArtistModule implements  ArtistaInterface{
  generos: string[];
  nombre: string;
  rutaImagen: string;
  id: string;
  constructor() { }

}
