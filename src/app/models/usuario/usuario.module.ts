import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsuarioInterface } from '../../interfaces/usuario.interface';

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class UsuarioModule implements UsuarioInterface{

  id: string;
  apellido: string;
  edad: number;
  nombre: string;
  sexo: string;
  correo: string;
  password: string;
  pais: string;
  avatar: string;

  constructor( ) { }



}
