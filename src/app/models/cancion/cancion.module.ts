import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CancionInterface } from '../../interfaces/cancion.interface';

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class CancionModule implements CancionInterface{
  artista: string;
  duracion: number;
  genero: string;
  gustada: boolean;
  id: string;
  idArtista: number;
  rutaImagen: string;
  titulo: string;
  nReproducciones: number;
}
