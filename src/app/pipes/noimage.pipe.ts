import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'noimage'
})
export class NoimagePipe implements PipeTransform {

  transform(imageURL: string): string {
    if ( imageURL )
    {
      return imageURL;
    } else {
      return 'assets/img/not-found.png';
    }
  }
}
