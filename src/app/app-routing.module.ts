import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { MiMusicaComponent } from './pages/mi-musica/mi-musica.component';
import { ArtistaPerfilComponent } from './pages/artista-perfil/artista-perfil.component';
import { BusquedaComponent } from './pages/busqueda/busqueda.component';
import { LoginComponent } from './pages/login/login.component';
import { RegistroComponent } from './pages/registro/registro.component';
import { AuthGuard } from './guards/auth.guard';
import { AdminGuard } from './guards/admin.guard';
import { GustosUsuarioComponent } from './pages/gustos-usuario/gustos-usuario.component';
import { EditarPerfilComponent } from './pages/editar-perfil/editar-perfil.component';
import { AdministradorComponent } from './pages/administrador/administrador.component';
import { ColaborativosComponent } from './pages/administrador/algoritmo/colaborativos/colaborativos.component';
import { ContenidoComponent } from './pages/administrador/algoritmo/contenido/contenido.component';
import { AlgoritmosComponent } from './pages/administrador/algoritmos/algoritmos.component';
import { LinearRegressionComponent } from './pages/administrador/algoritmo/linear-regression/linear-regression.component';
import { NaivebayesComponent } from './pages/administrador/algoritmo/naivebayes/naivebayes.component';
import { KnnComponent } from './pages/administrador/algoritmo/knn/knn.component';
import { DecisiontreeComponent } from './pages/administrador/algoritmo/decisiontree/decisiontree.component';
import { DeveloperPageComponent } from './pages/developer-page/developer-page.component';
import { CreadoParaTiComponent } from './pages/creado-para-ti/creado-para-ti.component';
import { FormularioUsuarioGuard } from './guards/formulario-usuario.guard';
import { KmeansComponent } from './pages/administrador/algoritmo/kmeans/kmeans.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent, canActivate: [FormularioUsuarioGuard, AuthGuard] },
  { path: 'aboutMe', component: DeveloperPageComponent },
  { path: 'profile', component:  MiMusicaComponent, canActivate: [FormularioUsuarioGuard, AuthGuard] },
  { path: 'artist/:id', component: ArtistaPerfilComponent, canActivate: [FormularioUsuarioGuard, AuthGuard] },
  { path: 'search/:termino', component: BusquedaComponent, canActivate: [FormularioUsuarioGuard, AuthGuard] },
  { path: 'login', component: LoginComponent },
  { path: 'registro', component: RegistroComponent },
  { path: 'formularioUsuario', component: GustosUsuarioComponent, canActivate: [FormularioUsuarioGuard, AuthGuard] },
  { path: 'creado-para-ti/:usuarioId', component: CreadoParaTiComponent, canActivate: [FormularioUsuarioGuard, AuthGuard] },
  { path: 'editProfile', component: EditarPerfilComponent, canActivate: [FormularioUsuarioGuard, AuthGuard] },
  { path: 'administrador', component: AdministradorComponent, canActivate: [AdminGuard] },
  { path: 'algoritmos', component: AlgoritmosComponent, canActivate: [AdminGuard] },
  { path: 'algoritmo/linearregression/:name', component: LinearRegressionComponent, canActivate: [AdminGuard] },
  { path: 'algoritmo/naivebayes/:name', component: NaivebayesComponent, canActivate: [AdminGuard] },
  { path: 'algoritmo/kmeans/:name', component: KmeansComponent, canActivate: [AdminGuard] },
  { path: 'algoritmo/knn/:name', component: KnnComponent, canActivate: [AdminGuard] },
  { path: 'algoritmoColab/:name', component: ColaborativosComponent, canActivate: [AdminGuard] },
  { path: 'algoritmoCont/:name', component: ContenidoComponent, canActivate: [AdminGuard] },

  { path: '**', pathMatch: 'full', redirectTo: (localStorage.getItem('admin')) ? 'administrador' : 'home'}
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
