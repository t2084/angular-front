import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { UsuariosService } from '../services/usuarios/usuarios.service';

@Injectable({
  providedIn: 'root'
})
export class FormularioUsuarioGuard implements CanActivate {
  constructor( private router: Router, private usuarioService: UsuariosService  ) {}

  canActivate(): boolean{

    this.usuarioService.cancionesGustadas(localStorage.getItem('correo')).subscribe(
      (data: []) => {
        if ( data.length == 0 ) {
          this.router.navigateByUrl("/formularioUsuario");
        }
      }
    );

    return true;
  }
}
