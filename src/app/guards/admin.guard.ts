import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AdminGuard implements CanActivate {
  constructor( private router: Router ) { }
  canActivate(  ): boolean {
    if ( localStorage.getItem('admin') !== '1') {
      this.router.navigateByUrl('login');
    }

    return true;
  }

}
