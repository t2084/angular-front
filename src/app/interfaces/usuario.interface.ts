export interface UsuarioInterface{
  nombre: string;
  apellido: string;
  edad: number;
  sexo: string;
  correo: string;
  password: string;
  pais: string;
}
