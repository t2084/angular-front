export interface CancionInterface{
  id: string;
  titulo: string;
  duracion: number;
  nReproducciones: number;
  rutaImagen: string;
  idArtista: number;
  artista: string;
  gustada: boolean;
  genero: string;
}
