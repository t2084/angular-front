export interface ArtistaInterface{
  nombre: string;
  rutaImagen: string;
  generos: string[];
  id: string;
}
