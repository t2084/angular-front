import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { HomeComponent } from './pages/home/home.component';
import { NavbarComponent } from './pages/navbar/navbar.component';
import { CancionComponent } from './pages/cancion/cancion.component';
import { MiMusicaComponent } from './pages/mi-musica/mi-musica.component';
import { HttpClientModule } from '@angular/common/http';
import { ArtistaPerfilComponent } from './pages/artista-perfil/artista-perfil.component';
import { BusquedaComponent } from './pages/busqueda/busqueda.component';
import { NoimagePipe } from './pipes/noimage.pipe';
import { FormsModule } from '@angular/forms';
import { LoginComponent } from './pages/login/login.component';
import { RegistroComponent } from './pages/registro/registro.component';
import { GustosUsuarioComponent } from './pages/gustos-usuario/gustos-usuario.component';
import { EditarPerfilComponent } from './pages/editar-perfil/editar-perfil.component';
import { ArtistaComponent } from './pages/artista/artista.component';
import { AdministradorComponent } from './pages/administrador/administrador.component';
import { NavbarAdminComponent } from './pages/administrador/navbar-admin/navbar-admin.component';
import { AlgoritmosComponent } from './pages/administrador/algoritmos/algoritmos.component';
import { ColaborativosComponent } from './pages/administrador/algoritmo/colaborativos/colaborativos.component';
import { ContenidoComponent } from './pages/administrador/algoritmo/contenido/contenido.component';
import { NovedadComponent } from './pages/novedad/novedad.component';
import { LinearRegressionComponent } from './pages/administrador/algoritmo/linear-regression/linear-regression.component';
import { KnnComponent } from './pages/administrador/algoritmo/knn/knn.component';
import { NaivebayesComponent } from './pages/administrador/algoritmo/naivebayes/naivebayes.component';
import { DecisiontreeComponent } from './pages/administrador/algoritmo/decisiontree/decisiontree.component';
import { StoreModule } from '@ngrx/store';
import { DeveloperPageComponent } from './pages/developer-page/developer-page.component';
import { FooterComponent } from './pages/footer/footer.component';
import { CreadoParaTiComponent } from './pages/creado-para-ti/creado-para-ti.component';
import { KmeansComponent } from './pages/administrador/algoritmo/kmeans/kmeans.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavbarComponent,
    CancionComponent,
    MiMusicaComponent,
    ArtistaPerfilComponent,
    BusquedaComponent,
    NoimagePipe,
    LoginComponent,
    RegistroComponent,
    GustosUsuarioComponent,
    EditarPerfilComponent,
    ArtistaComponent,
    AdministradorComponent,
    NavbarAdminComponent,
    AlgoritmosComponent,
    ColaborativosComponent,
    ContenidoComponent,
    NovedadComponent,
    LinearRegressionComponent,
    KnnComponent,
    NaivebayesComponent,
    DecisiontreeComponent,
    DeveloperPageComponent,
    FooterComponent,
    CreadoParaTiComponent,
    KmeansComponent,
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        FormsModule,
        StoreModule.forRoot({}, {})
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
